# Conectando seu banco de dados de Sqlite para PostreSQL

## Criando um `role` e database no PostgreSQL
1. Acesse o terminal interativo do PostgreSQL rodando o comando:
```
$ psql
```

2. Crie um role e o banco de dados que serão usados em sua aplicação com os comandos:
```
postgres=# CREATE USER <role_name> CREATEDB;
postgres=# CREATE DATABASE <database_name>;
postgres=# ALTER DATABASE <database_name> OWNER TO <role_name>;
```

## Configurando seu projeto em Rails
1. Substitua a `gem` do MySQL por `'pg'` no Gemfile
```
gem 'pg'
```

2. Atualize o arquivo `database.yml` com as devidas configurações:
```
# PostgreSQL. Versions 8.2 and up are supported.
#
# Install the pg driver:
#   gem install pg
# On Mac OS X with macports:
#   gem install pg -- --with-pg-config=/opt/local/lib/postgresql84/bin/pg_config
# On Windows:
#   gem install pg
#       Choose the win32 build.
#       Install PostgreSQL and put its /bin directory on your path.
#
# Configure Using Gemfile
# gem 'pg'
#
development:
  adapter: postgresql
  encoding: unicode
  database: <database_name>
  pool: 5
  username: <database_role>
  password:

  # Connect on a TCP socket. Omitted by default since the client uses a
  # domain socket that doesn't need configuration. Windows does not have
  # domain sockets, so uncomment these lines.
  #host: localhost
  #port: 5432

  # Schema search path. The server defaults to $user,public
  #schema_search_path: myapp,sharedapp,public

  # Minimum log levels, in increasing order:
  #   debug5, debug4, debug3, debug2, debug1,
  #   log, notice, warning, error, fatal, and panic
  # The server defaults to notice.
  #min_messages: warning

# Warning: The database defined as "test" will be erased and
# re-generated from your development database when you run "rake".
# Do not set this db to the same as development or production.
test:
  adapter: postgresql
  encoding: unicode
  database: <database_name>
  pool: 5
  username: <database_role>
  password:

production:
  adapter: postgresql
  encoding: unicode
  database: <database_name>
  pool: 5
  username: <database_role>
  password:
```

3. Execute o comando
```
$ rails db:migrate
```