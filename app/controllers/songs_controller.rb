class SongsController < ApplicationController
  before_action :set_playlist, only: [:create, :destroy]

  def create
    @song = @playlist.songs.build(song_params)

    if @song.save
      redirect_to playlist_path(@playlist)
    else
      render :new
    end
  end

  def destroy
    @song = @playlist.songs.find(params[:id])
    @song.destroy
    redirect_to playlist_path(@playlist)
  end
 
  private
  def song_params
    params.require(:song).permit(:name, :artist)
  end

  def set_playlist
    @playlist = Playlist.find(params[:playlist_id])
  end

end
