class PlaylistsController < ApplicationController

  before_action :set_playlist, only: [:show, :edit, :update, :destroy]

  def index
    @playlists = Playlist.all
  end

  def show
    @song = @playlist.songs.new
  end

  def new
    @playlist = Playlist.new
  end

  def edit
  end

  def create
    @playlist = Playlist.new(playlist_params)

    if @playlist.save
      redirect_to @playlist
    else
      render 'new'
    end
  
  end

  def update
    if @playlist.update(playlist_params)
      redirect_to @playlist
    else
      render 'edit'
    end
  end

  def destroy
    @playlist.destroy 
    redirect_to playlists_path
  end

  private
  def playlist_params
    params.require(:playlist).permit(:title, :description)
  end

  def set_playlist
    @playlist = Playlist.find(params[:id])
  end
  
end
