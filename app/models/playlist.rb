class Playlist < ApplicationRecord
  has_many :songs, dependent: :destroy
  validates :title, presence: true
end
